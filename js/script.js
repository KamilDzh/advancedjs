/* 1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript:
Прототипне наслідування дає доступ до властивостей об'єкта а сам прототип це об'єкт вiд якого спадковуються iншi об'єкти.
   2. Для чого потрібно викликати super() у конструкторі класу-нащадка?:
   Викликати super() потрібно для успадкування властивостей батьківсього конструктору. */


class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    get name() {
        return this._name;
    }
    get age() {
        return this._age;
    }
    get salary() {
        return this._salary;
    }

    set name(name) {
        this._name = name;
    }
    set age(age) {
        this._age = age;
    }
    set salary(salary) {
        this._salary = salary;
    }
}

class Programmer extends Employee {

    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }

    get salary(){
        return this._salary * 3;
    }

    showInfo() {
        console.log(`Name: ${this.name}, age ${this.age}
    salary: ${this.salary}
    languages: ${this._lang}`);
      }
    
}

firstProg = new Programmer('Kamil', 20, 4500, 'english, spain');
secondProg = new Programmer('Anton', 19, 5500, 'english, ukrainian');

firstProg.showInfo();
secondProg.showInfo();
